import { WishListItem, WishList } from './WishList'

it('can create a instace of a model', () => {
    const item = WishListItem.create({
        name: 'any name',
        price: 30.0,
        image: 'any image'
    })

    expect(item.price).toBe(30.0)
    expect(item.image).toBe('any image')
})

it('can create a wishlist', () => {
    const list = WishList.create({
        items: [
            {
                name: 'any name',
                price: 30.0
            }
        ]
    })
    expect(list.items.length).toBe(1)
    expect(list.items[0].price).toBe(30.0)
})
